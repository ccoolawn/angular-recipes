import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output()
  recipeWasSelected = new EventEmitter<Recipe>();

  recipes: Recipe[] = [
    new Recipe(
      'A Test Recipe',
      'A yummy delicious recipe',
      'https://drop.ndtv.com/albums/COOKS/pasta-vegetarian/pastaveg_640x480.jpg'
    ),
    new Recipe(
      'Dumplings',
      'Dumplings are Awesome!',
      'https://images.unsplash.com/photo-1534422298391-e4f8c172dddb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=9eb7a97f47b61a9f38ccc09c7f607d01&auto=format&fit=crop&w=1950&q=80'
    ),
    new Recipe(
      'Pasta Bolognese',
      'Italian Pasta Dishes!!!',
      'https://images.unsplash.com/photo-1530114722439-f5cbd4dad762?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e4f4a6fee4adc30b60515fb522061cd8&auto=format&fit=crop&w=934&q=80'
    ),
    new Recipe(
      'Salmon',
      'Salmon with Spinach & Garden Vegetable Relish',
      'https://images.unsplash.com/photo-1467003909585-2f8a72700288?ixlib=rb-0.3.5&s=02aa2b0d9e324bc335b561e6d3d0c98c&auto=format&fit=crop&w=934&q=80'
    ),
    new Recipe(
      'Buttered Mussels',
      'A succulent mussel dish with Citrus-Brown Butter Vinaigrette',
      'https://images.unsplash.com/photo-1514536338919-cd001f6dc6b3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=48f4f1385c293d8520c1f9c3233a98d6&auto=format&fit=crop&w=934&q=80'
    ),
    new Recipe(
      'The Epic Burger',
      'The Most Epic Burger Ever!!!!!!!',
      'https://images.unsplash.com/photo-1504185945330-7a3ca1380535?ixlib=rb-0.3.5&s=99aceb74ab1629f3f881089645976781&auto=format&fit=crop&w=921&q=80'
    )
  ];

  constructor() {}

  ngOnInit() {}

  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }
}
